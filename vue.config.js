module.exports = {
    // publicPath: "/aorus_vue",
    publicPath: "/",

    devServer: {
        port: 9000,
        open: true,
        https: false
    },

    pages: {
      index: {
        entry: 'src/home.js',
        template: 'src/pages/template/home.html',
        filename: 'index.html',
        chunks: ['chunk-vendors', 'chunk-common', 'index']
      }
    },

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: false
      }
    }
};
