import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import VueMeta from 'vue-meta'
import i18n from './i18n'
import VueHighlightJS from 'vue-highlightjs'

import "@/assets/css/vscod2015.css"
import '@/assets/css/style.css'


Vue.use(VueMeta, {
  
});

Vue.use(VueHighlightJS);

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')


