import Vue from 'vue'
import VueRouter from 'vue-router'
import i18n from '../i18n'
import Content from '../views/Content.vue'
import Index from '../views/Index.vue'
import SoftwareList from '../views/SoftwareList.vue'
import LessonList from '../views/Lesson/LessonList.vue'
import ReferenceList from '../views/ReferenceList.vue'
import Page404 from '../views/Page404.vue'

import CreateProject from '../views/Lesson/CreateProject.vue'
import Component from '../views/Lesson/Component.vue'
import Axios from '../views/Lesson/Axios.vue'
import Build from '../views/Lesson/Build.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },
  {
    path: '/:lang',
    component: Content,
    beforeEnter (to, from, next) {
      const lang = to.params.lang
      // 如果 URL 非 en、tw 則導至瀏覽器預設語言
      if (!['en', 'tw'].includes(lang)) {
        return next(i18n.locale)
      }
      // 如果 URL 有語系，則置換 i18n 語言
      if (i18n.locale !== lang) {
        i18n.locale = lang
      }
      return next()
    },
    children: [
      {
        path: '',
        name: 'Index',
        component: Index
      },
      {
        path: 'SoftwareList',
        name: 'SoftwareList',
        component: SoftwareList
      },
      {
        path: 'LessonList',
        name: 'LessonList',
        component: LessonList
      },
      //LessonList子項目 
          {
            path: 'LessonList/CreateProject',
            name: 'CreateProject',
            component: CreateProject
          },
          {
            path: 'LessonList/Component',
            name: 'Component',
            component: Component
          },
          {
            path: 'LessonList/Axios',
            name: 'Axios',
            component: Axios
          },
          {
            path: 'LessonList/Build',
            name: 'Build',
            component: Build
          },
      {
        path: 'ReferenceList',
        name: 'ReferenceList',
        component: ReferenceList
      },
      {
        path: '*',
        name: '404',
        component: Page404
      }
      
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  // mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
